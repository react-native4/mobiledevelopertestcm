// Native Base
import { Text, Card, CardItem, Icon } from 'native-base'

// React
import React from 'react'
import { TouchableOpacity, View, StyleSheet } from 'react-native'

// Shared
import { MainContainer, Colors } from '../../shared'

const Home = ({ navigation }) => {

    const cardItems = [
        {
            route: "Products",
            iconType: 'MaterialIcons',
            icon: "format-list-bulleted",
            iconColor: "blue",
            name: "Nuestros Productos",
            onPress: () => navigation.navigate('Productos')
        },
        {
            route: "Contact",
            iconType: 'MaterialIcons',
            icon: "phone",
            iconColor: "red",
            name: "Contáctenos",
            onPress: () => navigation.navigate('Contactenos')
        }
    ]

    const getCardItemStyle = (i) => {
        const cardItemGeneralStyle = {
            justifyContent: "space-between",
            paddingBottom: 10,
        }
        if (i === (cardItems.length - 1)) return cardItemGeneralStyle
        return {
            ...cardItemGeneralStyle,
            borderBottomWidth: 1,
            borderColor: Colors.primary.headerBackground
        }
    }

    return (
        <MainContainer>
            <Card>
                {
                    cardItems.map((item, i) =>
                        <TouchableOpacity key={i} onPress={() => item.onPress()}>
                            <CardItem style={getCardItemStyle(i)}>
                                <View style={styles.textContainer}>
                                    <Icon active name={item.icon} type={item.iconType} style={{ color: item.iconColor }} />
                                    <Text>{item.name}</Text>
                                </View>
                                <Icon name="arrow-forward" />
                            </CardItem>
                        </TouchableOpacity>
                    )
                }
            </Card>
        </MainContainer>
    )
}

const styles = StyleSheet.create({
    cardItem: {
        justifyContent: "space-between",
        paddingBottom: 10,
        borderBottomWidth: 1,
        borderColor: Colors.primary.headerBackground
    },
    textContainer: {
        flexDirection: "row"
    }
})

export default Home
