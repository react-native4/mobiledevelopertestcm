// Rxjs
import { BehaviorSubject } from 'rxjs'

const sessionObservable = new BehaviorSubject(false)

const logOut = () => {
    sessionObservable.next(true)
}

const SessionRxjs = {
    sessionObservable,
    logOut
}

export default SessionRxjs