enum ToastEnum {
    success = "success",
    alert = "warning",
    danger = "danger"
}

export default ToastEnum