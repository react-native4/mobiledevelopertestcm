class Product {
    Description: string
    ImageUrl: string
    Name: string

    constructor(Description: string, ImageUrl: string, Name: string) {
        this.Description = Description
        this.ImageUrl = ImageUrl
        this.Name = Name
    }
}

export default Product