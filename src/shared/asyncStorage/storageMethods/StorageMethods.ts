// React
import { AsyncStorage } from 'react-native';

/**
 * Setea item en storage
 * @param itemKey string
 * @param item any
 */
const setItem = async (itemKey: string, item: any): Promise<boolean> => {
    try {
        if (!itemKey || !item) {
            console.error('ItemKey o item no recibidos')
            return false
        }
        const itemString = JSON.stringify(item)
        await AsyncStorage.setItem(itemKey, itemString)
        return true
    } catch (e) {
        console.error('Error seteando item ' + e)
        return false
    }
}

/**
 * Obtiene item de storage
 * @param itemKey string
 */
const getItem = async (itemKey: string): Promise<any> => {
    try {
        if (!itemKey) return console.error('ItemKey no recibido')
        const item = await AsyncStorage.getItem(itemKey)
        if (!item) return console.error(`Item con llave ${itemKey} no encontrado`)
        return JSON.parse(item)
    } catch (e) {
        console.error('Error obteniendo item ' + e)
        return null
    }
}

/**
 * Elimina item de storage
 * @param itemKey string
 */
const deleteItem = async (itemKey: string): Promise<boolean> => {
    try {
        if (!itemKey) {
            console.error('ItemKey no recibido')
            return false
        }
        await AsyncStorage.removeItem(itemKey)
        return true
    } catch (e) {
        console.error('Error eliminando item' + e)
        return false
    }
}

const StorageMethods = {
    setItem,
    getItem,
    deleteItem
}

export default StorageMethods