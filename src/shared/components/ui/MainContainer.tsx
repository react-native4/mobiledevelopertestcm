// Native Base
import { Container } from 'native-base'

// React
import React from 'react'
import { StyleSheet } from 'react-native'

// Styles
import Colors from '../../styles/colors'

const MainContainer = ({ children }) => {
    return (
        <Container style={styles.mainContainer}>
            {children}
        </Container>
    )
}

const styles = StyleSheet.create({
    mainContainer: {
        backgroundColor: Colors.primary.mainBackground,
        margin: 10
    }
})

export default MainContainer
