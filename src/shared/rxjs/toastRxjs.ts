// Classes
import ToastClass from '../classes/toastClass'

// Enums
import ToastEnum from '../enums/toastEnum'

// Rxjs
import { BehaviorSubject } from 'rxjs'

const toastObservable = new BehaviorSubject(new ToastClass(ToastEnum.alert, null))

const showSuccessToast = (message: string) => {
    toastObservable.next(new ToastClass(ToastEnum.success, message))
}

const showAlertToast = (message: string) => {
    toastObservable.next(new ToastClass(ToastEnum.alert, message))
}

const showDangerToast = (message: string) => {
    toastObservable.next(new ToastClass(ToastEnum.danger, message))
}

const ToastRxjs = {
    toastObservable,
    showSuccessToast,
    showAlertToast,
    showDangerToast
}

export default ToastRxjs