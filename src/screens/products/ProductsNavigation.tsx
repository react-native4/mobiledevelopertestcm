// Components
import Products from './Products'

const ProductsNavigation = {
    "Productos": {
        screen: Products,
        navigationOptions: ({ navigation }) => ({
            title: "Nuestros Productos",
        })
    }
}

export default ProductsNavigation