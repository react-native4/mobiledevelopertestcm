//Async Storage
import StorageMethods from './storageMethods/StorageMethods'

const sessionKey = '_Session_'

/**
 * Guarda sesión
 * @param session 
 */
const storeSession = async (session: any): Promise<boolean | void> => {
    return StorageMethods.setItem(sessionKey, session)
        .then(sessionAdded => sessionAdded)
        .catch(e => console.error(e))
}

/**
 * Obtiene sesión
 */
const getSession = async (): Promise<any | void> => {
    return StorageMethods.getItem(sessionKey)
        .then(session => session)
        .catch(error => console.error('Error obteniendo sesión' + error))
}

/**
 * Elimina sesión
 */
const deleteSession = async (): Promise<boolean | void> => {
    return StorageMethods.deleteItem(sessionKey)
        .then(sessionRemoved => sessionRemoved)
        .catch(e => console.error(e))
}

const SessionStorage = {
    storeSession,
    getSession,
    deleteSession
}

export default SessionStorage