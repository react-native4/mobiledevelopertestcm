// Native Base
import { Card, CardItem, Left, Title, Text } from 'native-base'

// React
import React from 'react'
import { Image, StyleSheet, TouchableOpacity } from 'react-native'

// Styles
import Colors from '../../styles/colors'


interface Props {
    title: string,
    subtitle: string,
    image: string,
}

const CardComponent = (props: Props) => {

    const { title, subtitle, image } = props

    return (
        <Card style={styles.cardContainer}>
            <CardItem style={styles.cardHeader}>
                <Text style={styles.cardTitle}>{title}</Text>
                <Text style={styles.cardSubtitle}>{subtitle}</Text>
            </CardItem>
            <CardItem cardBody>
                <Image source={{ uri: image }} style={styles.cardImage} />
            </CardItem>
        </Card>
    )
}

const styles = StyleSheet.create({
    cardContainer: {
        borderWidth: 1,
        borderColor: Colors.primary.headerBackground,
        marginBottom: 10
    },
    cardHeader: {
        backgroundColor: Colors.primary.cardBackground,
        flexDirection: 'column',
        alignItems: 'flex-start',
        borderBottomWidth: 1,
        borderBottomColor: Colors.primary.headerBackground
    },
    cardImage: {
        height: 200,
        width: null,
        flex: 1,
        resizeMode: "contain"
    },
    cardTitle: {
        color: Colors.primary.cardText,
        fontWeight: "bold",
        fontSize: 20
    },
    cardSubtitle: {
        color: Colors.primary.cardText
    }
})

export default CardComponent
