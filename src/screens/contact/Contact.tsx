// Maps
import MapView, { Marker } from 'react-native-maps'

// Native Base
import { Text, Icon, Card, CardItem } from 'native-base'

// React
import React, { useState } from 'react'
import { StyleSheet, View } from 'react-native';

// Shared
import { MainContainer } from '../../shared'

const Contact = () => {

    const [locationState] = useState({
        latitude: 4.590224,
        longitude: -74.127222,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
    })

    const [mapMarkers] = useState([
        {
            latitude: 4.590224,
            longitude: -74.127222,
            title: 'Ubicación',
            subtitle: 'Dg 40B #34D-09 S'
        }
    ])

    const contactInfo = [
        {
            title: 'Correo',
            value: 'mateus_09@gmail.com',
            iconType: 'MaterialIcons',
            icon: "email",
            iconColor: "black",
        },
        {
            title: 'Whatsapp',
            value: '+57 320 951 3415',
            iconType: 'FontAwesome',
            icon: "whatsapp",
            iconColor: "green",
        },
        {
            title: 'Dirección',
            value: 'Dg 40B #34D-09 S',
            iconType: 'MaterialIcons',
            icon: "near-me",
            iconColor: "blue",
        }
    ]

    return (
        <MainContainer>
            <Card style={styles.infoCard}>
                {
                    contactInfo.map((item, i) =>
                        <CardItem style={styles.contactDataItem} key={i}>
                            <Icon type={item.iconType} name={item.icon} style={{ color: item.iconColor }} />
                            <Text>{item.title}: {item.value}</Text>
                        </CardItem>
                    )
                }
            </Card>
            <View style={styles.container}>
                <MapView
                    style={styles.mapStyle}
                    region={locationState}

                    showsUserLocation
                    showsMyLocationButton
                    zoomEnabled
                >
                    {
                        mapMarkers.map((marker, i) =>
                            <Marker
                                key={i}
                                coordinate={{
                                    latitude: marker.latitude,
                                    longitude: marker.longitude
                                }}
                                title={marker.title}
                                description={marker.subtitle}
                            />
                        )
                    }
                </MapView>
            </View>
        </MainContainer>
    )
}

const styles = StyleSheet.create({
    infoCard: {
        marginBottom: 10
    },
    contactDataItem: {
        flexDirection: "row"
    },
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    mapStyle: {
        width: "100%",
        height: "100%",
    },
});

export default Contact
