// Components
import Home from './Home'

//Native Base
import { Icon } from 'native-base'

//React
import React from 'react'
import { TouchableOpacity, StyleSheet } from 'react-native'

// Shared
import { Colors, SessionRxjs } from '../../shared'

const HomeNavigation = {
    Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
            title: "Inicio",
            headerLeft: () => null,
            headerRight: () => (
                <TouchableOpacity onPress={() => SessionRxjs.logOut()}>
                    <Icon style={styles.logoutButton} type="MaterialIcons" name="keyboard-return" />
                </TouchableOpacity>
            ),
        })
    }
}

const styles = StyleSheet.create({
    logoutButton: {
        color: Colors.primary.headerText,
        marginRight: 10
    }
})

export default HomeNavigation