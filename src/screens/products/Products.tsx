// React
import React, { useEffect, useState } from 'react'
import { FlatList } from 'react-native'

// Shared
import { MainContainer, ScreenLoaderRxjs, ToastRxjs, ProductServices, ProductClass, Card } from '../../shared'

const Products = () => {

    const [productListState, setProductListState] = useState(new Array<ProductClass>())

    useEffect(() => {
        getAllProducts()
    }, [])

    /**
     * Obtiene todos los productos
     */
    const getAllProducts = async () => {
        ScreenLoaderRxjs.showLoader()
        const allProducts = await ProductServices.getProducts()
        ScreenLoaderRxjs.hideLoader()
        if (!allProducts) return ToastRxjs.showDangerToast('Error obteniendo productos')
        setProductListState(allProducts)
    }



    return (
        <MainContainer>
            <FlatList
                data={productListState}
                renderItem={({ item }) => <Card title={item.Description} subtitle={item.Name} image={item.ImageUrl} />}
                keyExtractor={(item, i) => i.toString()}
            />
        </MainContainer>
    )
}

export default Products
