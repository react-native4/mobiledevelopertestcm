// Classes
import ScreenLoader from '../../classes/screenLoader.class'

// Native Base
import { Spinner, Text } from "native-base";

// React
import React from 'react'
import { StyleSheet, View, Modal } from 'react-native'

// Styles
import Colors from '../../styles/colors'

interface Props {
    screenLoader: ScreenLoader
}

const ScreenLoaderComponent = (props: Props) => {
    return (
        props.screenLoader.show ?
            <Modal animationType="fade" transparent={true} visible={true}>
                <View style={styles.screenLoader}>
                    <Spinner
                        color={Colors.primary.headerText}
                        style={styles.spinner}
                    />
                    <Text style={styles.text}>{props.screenLoader.message}</Text>
                </View>
            </Modal>
            :
            null
    )
}

const styles = StyleSheet.create({
    screenLoader: {
        backgroundColor: Colors.primary.headerBackground,
        width: "100%",
        height: "100%",
        justifyContent: "center",
        alignItems: "center"
    },
    spinner: {
        height: 80
    },
    image: {
        width: 300,
        height: 100
    },
    textContainer: {
        backgroundColor: "white"
    },
    text: {
        textAlign: "center",
        paddingHorizontal: 70,
        fontSize: 22
    }
});



export default ScreenLoaderComponent