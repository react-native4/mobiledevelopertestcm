// Formik
import { Formik } from "formik";

// Native Base
import { Container, Text, Button, Input, Item, Label } from 'native-base'

// React
import React, { useState, useEffect, Fragment } from 'react'
import { StyleSheet, View, Image } from 'react-native'

// Shared
import { ToastRxjs, SecurityServices, UserClass, ErrorMessage, SessionStorage, Colors, SessionRxjs } from '../../shared'

// Yup
import * as Yup from "yup";

const Login = ({ navigation }) => {
    const [initialValuesState] = useState(new UserClass("", ""));

    useEffect(() => {
        setUpSessionSubscriber()
    }, [])

    /**
     * Metodo encargado de logueo
     * @param user Usuario y contraseña
     */
    const handleUserLogin = async (user: UserClass) => {
        //Consumir serivio de logueo
        const session = await SecurityServices.authUser(user)
        if (!session) return ToastRxjs.showDangerToast('No se pudo validar sesión')
        //Guardar sesión en storage
        await SessionStorage.storeSession({ ...session, user: user.user })
        ToastRxjs.showSuccessToast('Bienvenido!')
        //Navegar a home
        navigation.navigate('Home')
    }

    /**
    * Subscriptor de Sesión
    */
    const setUpSessionSubscriber = () => {
        SessionRxjs.sessionObservable.subscribe((logout) => {
            if (!logout) return
            SessionStorage.deleteSession()
            navigation.navigate('Login')
        })
    }


    const SignupSchema = Yup.object().shape({
        user: Yup.string().required("Campo Requerido"),
        password: Yup.string().required("Campo Requerido")
    });

    return (
        <Container style={styles.loginContainer}>
            <Formik
                initialValues={initialValuesState}
                validationSchema={SignupSchema}
                onSubmit={user => handleUserLogin(user)}
            >
                {({
                    values,
                    errors,
                    handleChange,
                    setFieldTouched,
                    isValid,
                    handleSubmit
                }) => (
                        <Fragment>
                            <Image
                                style={styles.loginImage}
                                source={require("../../../assets/loginImage.png")}
                            />
                            <View style={styles.inputsContainer}>
                                <Item floatingLabel>
                                    <Label>Usuario</Label>
                                    <Input
                                        placeholder={'Usuario'}
                                        onChangeText={handleChange('user')}
                                        onBlur={() => setFieldTouched('user')}
                                        value={values.user}
                                    />
                                </Item>
                                <ErrorMessage message={errors.user} />
                                <Item floatingLabel>
                                    <Label>Contraseña</Label>
                                    <Input
                                        placeholder={'Contraseña'}
                                        onChangeText={handleChange('password')}
                                        onBlur={() => setFieldTouched('password')}
                                        value={values.password}
                                        secureTextEntry={true}
                                    />
                                </Item>
                                <ErrorMessage message={errors.password} />
                            </View>
                            <Button onPress={() => {
                                if (!isValid) return
                                handleSubmit()
                            }
                            } success>
                                <Text>Iniciar Sesión</Text>
                            </Button>
                        </Fragment>
                    )}
            </Formik>
        </Container>
    )
}


const styles = StyleSheet.create({
    loginContainer: {
        height: '100%',
        alignItems: "center",
        justifyContent: 'center',
        padding: 40,
        backgroundColor: Colors.primary.mainBackground
    },
    loginImage: {
        height: 180,
        resizeMode: "contain"
    },
    inputsContainer: {
        width: 300,
        marginTop: 30,
        marginBottom: 20
    }
})

export default Login
