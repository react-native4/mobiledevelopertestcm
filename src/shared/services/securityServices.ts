// axios
import axios from 'axios'

// class
import UserClass from '../classes/userClass'

// config
import Config from '../config/config'

// rxjs
import ScreenLoaderRxjs from '../rxjs/screenLoaderRxjs'
import ToastRxjs from '../rxjs/toastRxjs'

const authUser = async (user: UserClass): Promise<any> => {
    ScreenLoaderRxjs.showLoader()
    return await axios.post(`${Config.baseURL}/login`, user).then(res => {
        ScreenLoaderRxjs.hideLoader()
        return res.data
    }).catch(e => {
        ScreenLoaderRxjs.hideLoader()
        ToastRxjs.showDangerToast(e.toString())
        console.error(e)
    })
}


const SecurityServices = {
    authUser
}

export default SecurityServices