// React
import React from 'react'
import { StyleSheet, Text } from 'react-native'

interface Props {
    message: string
}

const ErrorMessage = (props: Props) => {

    return <Text style={styles.errorMessage}>{props.message}</Text>
}

const styles = StyleSheet.create({
    errorMessage: {
        color: 'red'
    }
})

export default ErrorMessage
