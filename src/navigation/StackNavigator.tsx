// Navigations
import SecurityNavigation from '../screens/security/SecurityNavigation'
import HomeNavigation from '../screens/home/HomeNavigation'
import ProductsNavigation from '../screens/products/ProductsNavigation'
import ContactNavigation from '../screens/contact/ContactNavigation'

// React Navigation
import { createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'

// Shared
import { Colors } from '../shared'

const RouteConfigs = {
    ...SecurityNavigation,
    ...HomeNavigation,
    ...ProductsNavigation,
    ...ContactNavigation
}

const StackNavigatorConfig = {
    initialRouteName: "Login",
    defaultNavigationOptions: {
        headerStyle: { backgroundColor: Colors.primary.headerBackground },
        headerTintColor: Colors.primary.headerText
    }
}

const StackNavigator = createStackNavigator(RouteConfigs, StackNavigatorConfig)

export default createAppContainer(StackNavigator)
