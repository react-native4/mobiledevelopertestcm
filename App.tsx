// Native Base
import { Root, Toast } from 'native-base';
import * as Font from 'expo-font';
import { Ionicons } from '@expo/vector-icons';

// React
import React, { useEffect, useState, Fragment } from 'react';
import { KeyboardAvoidingView } from 'react-native';

// Shared
import { ScreenLoaderRxjs, ScreenLoaderComponent, ScreenLoader, ToastRxjs, ToastClass } from './src/shared'

// Stack Navigator
import StackNavigator from './src/navigation/StackNavigator'

export default function App() {
  // Loader
  const [screenLoaderState, setScreenLoaderState] = useState(new ScreenLoader(false, null))
  // Has native base fonts rendered
  const [nativeBaseFontsState, setNativeBaseFontsState] = useState(false)

  /**
   * Al cargar pagina obtener dependencias native base
   */
  useEffect(() => {
    setUpNativeBaseFonts()
  }, [])

  /**
   * Al obtener dependencias native base
   * obtener loader y toast subscribers
   */
  useEffect(() => {
    if (!nativeBaseFontsState) return
    setUpToastSubscriber()
    setScreenLoaerSubscriber()
  }, [nativeBaseFontsState])
  /**
   * Metodo encargado de setear fonts de native base
   */
  const setUpNativeBaseFonts = async () => {
    await Font.loadAsync({
      Roboto: require('native-base/Fonts/Roboto.ttf'),
      Roboto_medium: require('native-base/Fonts/Roboto_medium.ttf'),
      ...Ionicons.font,
    });
    setNativeBaseFontsState(true)
    ScreenLoaderRxjs.hideLoader()
  }

  /**
   * Subcriptor para loader de app
   */
  const setScreenLoaerSubscriber = () => {
    ScreenLoaderRxjs.screenLoaderObservable.subscribe((screenLoader: ScreenLoader) => {
      setScreenLoaderState(screenLoader)
    })
  }

  /**
   * Subscriptor para mensajes de toast
   */
  const setUpToastSubscriber = () => {
    ToastRxjs.toastObservable.subscribe((toast: ToastClass) => {
      if (!toast.message) return
      Toast.show({
        text: toast.message,
        type: toast.type,
        position: "bottom",
        duration: 2500
      })
    })
  }


  return (
    <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
      <Root>
        {
          nativeBaseFontsState ?
            <Fragment>
              <StackNavigator />
              <ScreenLoaderComponent screenLoader={screenLoaderState} />
            </Fragment>
            :
            <ScreenLoaderComponent screenLoader={new ScreenLoader(true, null)} />
        }
      </Root>
    </KeyboardAvoidingView>
  );
}