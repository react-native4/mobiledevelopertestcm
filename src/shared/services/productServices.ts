// Axios
import axios from 'axios'

// Classes
import ProductClass from '../classes/productClass'

// Config
import Config from '../config/config'

// Rxjs
import ToastRxjs from '../rxjs/toastRxjs'

/**
 * Obtiene todos los productos
 */
const getProducts = async (): Promise<Array<ProductClass> | void> => {
    return await axios.post(`${Config.baseURL}/GetProductsData`)
        .then(res => res.data)
        .catch(e => {
            ToastRxjs.showDangerToast('Error al obtener productos')
            console.error('Error al obtener productos' + e)
        })
}

const ProductServices = {
    getProducts
}

export default ProductServices