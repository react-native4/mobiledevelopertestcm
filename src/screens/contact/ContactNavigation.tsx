// Components
import Contact from './Contact'

const ContactNavigation = {
    "Contactenos": {
        screen: Contact,
        navigationOptions: ({ navigation }) => ({
            title: "Contáctenos",
        })
    }
}

export default ContactNavigation