// Classes
import ScreenLoader from '../classes/screenLoader.class'

// Rxjs
import { BehaviorSubject } from 'rxjs'

const screenLoaderObservable = new BehaviorSubject(new ScreenLoader(true, null))

const showLoader = (message?: string) => {
    screenLoaderObservable.next(new ScreenLoader(true, message))
}

const hideLoader = () => {
    screenLoaderObservable.next(new ScreenLoader(false, null))
}

const ScreenLoaderRXJS = {
    screenLoaderObservable,
    showLoader,
    hideLoader
}

export default ScreenLoaderRXJS