// Components
import Login from './Login'

const SecurityNavigation = {
    Login: {
        screen: Login,
        navigationOptions: { headerShown: false }
    }
}

export default SecurityNavigation