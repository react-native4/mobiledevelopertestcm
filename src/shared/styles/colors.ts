const primary = {
    mainBackground: '#EBF2FA',
    mainText: 'black',
    headerBackground: '#427AA1',
    headerText: '#F7FFF7',
    cardBackground: '#F7FFF7',
    cardText: 'black'
}

const Colors = {
    primary
}

export default Colors