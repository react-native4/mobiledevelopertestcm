// Enum
import ToastEnum from '../enums/toastEnum'

class ToastClass {
    type: ToastEnum
    message: string

    constructor(type: ToastEnum, message: string) {
        this.type = type
        this.message = message
    }
}

export default ToastClass